#pragma once
class Item
{
public:
	Item(int map[5][5], int type, bool intengible);
	Item(int map[5][5], int x, int y, int type, bool intengible);
	~Item();

	static void Move(int map[5][5], int xD, int yD, int xA, int yA);
	static void Remove(int map[5][5], int x, int y);

	int _x;
	int _y;
	int _type;

protected:
	void Add(int map[5][5], int x, int y, int type);

private:
	bool _intengible;
};

