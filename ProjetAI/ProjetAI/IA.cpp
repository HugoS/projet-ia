#include "IA.h"
#include <iostream>
#include <list>

IA::IA() {
	// constructeur

	depart.cout_f = 0;
	depart.cout_g = 0;
	depart.cout_h = 0;
	depart.parent.first = 0;
	depart.parent.second = 0;

	arrivee[0] = 4;
	arrivee[1] = 4;
}

/* calcule la distance entre les points (x1,y1) et (x2,y2) */
float distance(int x1, int y1, int x2, int y2) {
	/* distance euclidienne */
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

bool IA::dejaPresentDansListe(std::pair<int, int> n, IA::l_noeud& l) {
	l_noeud::iterator i = l.find(n);
	if (i == l.end())
		return false;
	else
		return true;
}

void IA::ajouterCasesAdjacentes(std::pair<int, int>& n, int map[5][5]) {
	IA::noeud tmp;

	//std::cout << "Case adjacente de : " << n.first << " " << n.second << std::endl;

	/* on met tous les noeuds adjacents dans la liste ouverte (+v�rif) */
	for (int i = n.first - 1; i <= n.first + 1; i++) {
		if ((i < 0) || (i >= 5))  /* en dehors de du tableau, on oublie */
			continue;
		for (int j = n.second - 1; j <= n.second + 1; j++) {
			if ((j < 0) || (j >= 5))   /* en dehors du tableau, on oublie */
				continue;
			if ((i == n.first) && (j == n.second))  /* case actuelle n, on oublie */
				continue;

			if ((map[i][j] == 1) || (map[i][j] == 2)) // s'il y a un pi�ge
				continue;

			std::pair<int, int> it(i, j);
			if (!dejaPresentDansListe(it, listeFermee)) {
				/* le noeud n'est pas d�j� pr�sent dans la liste ferm�e */

				/* calcul du cout G du noeud en cours d'�tude : cout du parent + distance jusqu'au parent */
				if (map[i][j] == 3)
				{
					// dans le cas de la boue (un ralentisseur)
					tmp.cout_g = listeFermee[n].cout_g + distance(i, j, n.first, n.second) + 1;
				}
				else {
					tmp.cout_g = listeFermee[n].cout_g + distance(i, j, n.first, n.second);
				}

				/* calcul du cout H du noeud � la destination */
				tmp.cout_h = distance(i, j, arrivee[0], arrivee[1]);
				tmp.cout_f = tmp.cout_g + tmp.cout_h;
				tmp.parent = n;

				if (dejaPresentDansListe(it, listeOuverte)) {
					/* le noeud est d�j� pr�sent dans la liste ouverte, il faut comparer les couts */
					if (tmp.cout_f < listeOuverte[it].cout_f) {
						/* si le nouveau chemin est meilleur, on met � jour */
						listeOuverte[it] = tmp;
					}

					/* le noeud courant a un moins bon chemin, on ne change rien */
				}
				else {
					/* le noeud n'est pas pr�sent dans la liste ouverte, on l'y ajoute */
					listeOuverte[std::pair<int, int>(i, j)] = tmp;
				}
			}
		}
	}
}

std::pair<int, int> IA::meilleurNoeud(l_noeud& l) {
	float coutf = l.begin()->second.cout_f;
	std::pair<int, int> noeud = l.begin()->first;

	for (l_noeud::iterator i = l.begin(); i != l.end(); i++) {
		if (i->second.cout_f < coutf) {
			coutf = i->second.cout_f;
			noeud = i->first;
		}
	}
	return noeud;
}

void IA::ajouterListeFermee(std::pair<int, int>& p) {
	IA::noeud& n = listeOuverte[p];
	listeFermee[p] = n;

	//std::cout << "Liste ferm�e : " << p.first << " " << p.second << std::endl;

	/* il faut le supprimer de la liste ouverte, ce n'est plus une solution explorable */
	if (listeOuverte.erase(p) == 0)
		std::cerr << "Erreur, le noeud n'appara�t pas dans la liste ouverte, impossible � supprimer" << std::endl;
	return;
}

void IA::retrouverChemin() {
	/* l'arriv�e est le dernier �l�ment de la liste ferm�e */
	IA::noeud& tmp = listeFermee[std::pair<int, int>(arrivee[0], arrivee[1])];

	//std::cout << "Retrouver le chemin" << std::endl;

	struct point n;
	std::pair<int, int> prec;
	n.x = arrivee[0];
	n.y = arrivee[1];
	prec.first = tmp.parent.first;
	prec.second = tmp.parent.second;
	chemin.push_front(n);

	while (prec != std::pair<int, int>(depart.parent.first, depart.parent.first)) {
		n.x = prec.first;
		n.y = prec.second;
		chemin.push_front(n);

		tmp = listeFermee[tmp.parent];
		prec.first = tmp.parent.first;
		prec.second = tmp.parent.second;

		//std::cout << "n : " << n.x << " " << n.y << std::endl;
		//std::cout << "prec : " << prec.first << " " << prec.second << std::endl;
	}
}

void IA::PathFinding(int map[5][5]) {
	// pathfinding de type a*
	depart.parent.first = 0;
	depart.parent.second = 0;

	std::pair<int, int> courant;

	/* d�roulement de l'algo A* */

	/* initialisation du noeud courant */
	courant.first = 0;
	courant.second = 0;


	/* ajout de courant dans la liste ouverte */
	listeOuverte[courant] = depart;
	ajouterListeFermee(courant);
	ajouterCasesAdjacentes(courant, map);

	/* tant que la destination n'a pas �t� atteinte et qu'il reste des noeuds � explorer dans la liste ouverte */
	while (!((courant.first == arrivee[0]) && (courant.second == arrivee[1])) && (!listeOuverte.empty()))
	{
		/* on cherche le meilleur noeud de la liste ouverte, on sait qu'elle n'est pas vide donc il existe */
		courant = meilleurNoeud(listeOuverte);

		/* on le passe dans la liste ferm�e, il ne peut pas d�j� y �tre */
		ajouterListeFermee(courant);

		/* on recommence la recherche des noeuds adjacents */
		ajouterCasesAdjacentes(courant, map);
	}

	/* si la destination est atteinte, on remonte le chemin */
	if ((courant.first == arrivee[0]) && (courant.second == arrivee[1])) {
		retrouverChemin();

		std::list<IA::point>::iterator i;
		for (i = chemin.begin(); i != chemin.end(); i++)
		{
			point a = *i;
			map[a.x][a.y] = 4;
		}
	}
	else {
		/* pas de solution */
		std::cout << "Pas de solution";
	}
}

IA::~IA() {
	// destructeur
}