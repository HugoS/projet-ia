#include "Item.h"

Item::Item(int map[5][5], int type, bool intengible) {
	_type = type;
	_intengible = intengible;

	//Add(map, _x, _y, type);
}

Item::Item(int map[5][5], int x, int y, int type, bool intengible) {
	_x = x;
	_y = y;
	_type = type;
	_intengible = intengible;

	Add(map, _x, _y, type);
}

void Item::Add(int map[5][5], int x, int y, int type) {
	map[x][y] = type;
}

void Item::Move(int map[5][5], int xD, int yD, int xA, int yA) {
	map[xA][yA] = map[xD][yD];
	map[xD][yD] = 0;
}

void Item::Remove(int map[5][5], int x, int y) {
	map[x][y] = 0;
}

Item::~Item() {
	delete this;
}