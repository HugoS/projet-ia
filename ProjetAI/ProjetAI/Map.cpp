#include "Map.h"

Map::Map() {
	// contructor
	Init();
}

void Map::Init() {
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			MyMap[i][j] = 0;
		}
	}

	MyMap[0][0] = 4;
}

void Map::Game() {
	// boucle de gameplay
	std::list<Item*> listObstacle;
	std::string allCommand[6];
	std::string entree = "";

	int x;
	int y;
	int xD;
	int yD;
	int xA;
	int yA;

	std::cout << "Commands : " << std::endl;
	std::cout << "- add : add item" << std::endl;
	std::cout << "- move : move item" << std::endl;
	std::cout << "- remove : rmove item" << std::endl;
	std::cout << "- reset : reset map" << std::endl;
	std::cout << "- help : watch commands" << std::endl;


	std::cout << "Next command : " << std::endl;
	std::cin >> entree;


	while (entree != "end")
	{
		IA *ia = new IA();
		while (entree != "start")
		{
			if (entree == "add")
			{
				// add item
				std::cout << "Coordonnee x ? ";
				std::cin >> x;
				std::cout << "Coordonnee y ? ";
				std::cin >> y;

				std::cout << "Quel type d'objet ? (mur ou piege ou boue) ";
				std::cin >> entree;
				if (entree == "mur")
				{
					listObstacle.push_back(new Wall(MyMap, x, y, 1, false));
				}
				else if (entree == "piege") {
					listObstacle.push_back(new Trap(MyMap, x, y, 2, false));
				}
				else if (entree == "boue") {
					listObstacle.push_back(new Boue(MyMap, x, y, 3, true));
				}

				Affichage(MyMap);
			}
			else if (entree == "move")
			{
				// move item
				std::cout << "Coordonnee x de l'objet ? ";
				std::cin >> xD;
				std::cout << "Coordonnee y de l'objet ? ";
				std::cin >> yD;
				std::cout << "Nouvelle coordonnee x ? ";
				std::cin >> xA;
				std::cout << "Nouvelle coordonnee y ? ";
				std::cin >> yA;

				Item::Move(MyMap, xD, yD, xA, yA);

				Affichage(MyMap);
			}
			else if (entree == "remove")
			{
				// remove item
				std::cout << "Coordonnee x de l'objet � supprimer ? ";
				std::cin >> x;
				std::cout << "Coordonnee y de l'objet � supprimer ? ";
				std::cin >> y;
				Item::Remove(MyMap, x, y);
			}
			else if (entree == "help")
			{
				std::cout << "Commands : " << std::endl;
				std::cout << "- add : add item" << std::endl;
				std::cout << "- move : move item" << std::endl;
				std::cout << "- remove : rmove item" << std::endl;
				std::cout << "- reset : reset map" << std::endl;
			}
			else if (entree == "reset")
			{
				Init();
				listObstacle.clear();
			}

			std::cout << "Next command : " << std::endl;
			std::cin >> entree;
		}
		// d�marrer le pathFinding
		ia->PathFinding(MyMap);
		Affichage(MyMap);
		Init();
		//MapClean(listObstacle);
		delete ia;

		std::cout << "Next command : " << std::endl;
		std::cin >> entree;

	}
}

void Map::Affichage(int map[5][5]) {
	for (int i = 0; i < 5; i++)
	{
		std::cout << "|";
		for (int j = 0; j < 5; j++)
		{
			if (map[i][j] != 0)
			{
				std::cout << map[i][j];
			}
			else {
				std::cout << " ";
			}
			std::cout << "|";
		}
		std::cout << std::endl;
	}
}

/*void Map::MapClean(std::list<Item*> listObstacle) {
	for (std::list<Item*>::iterator i = listObstacle.begin(); i != listObstacle.end(); i++)
	{
		MyMap[i->_x][i->_y] = i->_type;
	}
}*/

Map::~Map() {
	// destructor
}
