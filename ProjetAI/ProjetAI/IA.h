#pragma once
#include <iostream>
#include <list>
#include <map>

class IA
{
public:
	IA();
	~IA();

	void PathFinding(int map[5][5]);


private:
	// variable pour le pathfinding
	struct noeud {
		float cout_g; // le cout pour aller du point de d�part au n�ud consid�r�
		float cout_h; // le cout pour aller du n�ud consid�r� au point de destination
		float cout_f; // somme des pr�c�dents

		std::pair<int, int> parent;    // 'adresse' du parent (qui sera toujours dans la map ferm�e)
	};

	struct point {
		int x;
		int y;
	};

	//int depart[2]; // coordonn�es du point de d�part
	int arrivee[2]; // coordonn�es du point d'arriv�e
	noeud depart;

	typedef std::map <std::pair <int, int>, noeud> l_noeud;
	l_noeud listeOuverte;
	l_noeud listeFermee;
	std::list<point> chemin;

	// fonctions pour le pathfinding
	bool dejaPresentDansListe(std::pair<int, int> n, IA::l_noeud& l);
	void ajouterCasesAdjacentes(std::pair<int, int>& n, int map[5][5]);
	std::pair<int, int> meilleurNoeud(l_noeud& l);
	void ajouterListeFermee(std::pair<int, int>& p);
	void retrouverChemin();
};

